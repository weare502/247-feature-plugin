<?php
/**
 * Plugin Name: 24/7 Feature Plugin
 * Description: Site Specific Features for this website. DO NOT DEACTIVATE OR DELETE.
 * Version: 1.0
 * Author: 502 Media Group
 * Author URI: http://502mediagroup.com
 *
 * @package  twentyfourseven
 */

/**
 * Registers a new post type
 */
function twentyfourseven_register_locations_post_type(){

	$labels = array(
		'name'                => __( 'Locations', 'twentyfourseven' ),
		'singular_name'       => __( 'Location', 'twentyfourseven' ),
		'add_new'             => _x( 'Add New Location', 'twentyfourseven', 'twentyfourseven' ),
		'add_new_item'        => __( 'Add New Location', 'twentyfourseven' ),
		'edit_item'           => __( 'Edit Location', 'twentyfourseven' ),
		'new_item'            => __( 'New Location', 'twentyfourseven' ),
		'view_item'           => __( 'View Location', 'twentyfourseven' ),
		'search_items'        => __( 'Search Locations', 'twentyfourseven' ),
		'not_found'           => __( 'No Locations found', 'twentyfourseven' ),
		'not_found_in_trash'  => __( 'No Locations found in Trash', 'twentyfourseven' ),
		'parent_item_colon'   => __( 'Parent Location:', 'twentyfourseven' ),
		'menu_name'           => __( 'Locations', 'twentyfourseven' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-store',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'show_in_rest'		  => true,
		'rewrite'             => array( 'slug' => 'locations' ),
		'capability_type'     => 'location',
		'map_meta_cap' 		  => true,
		'supports'            => array(
			'title',
			'editor',
			'thumbnail',
			'excerpt',
		),
	);

	register_post_type( 'location', $args );
}

add_action( 'init', 'twentyfourseven_register_locations_post_type' );

/**
 * Registers Reward Cards Post Type.
 */
function twentyfourseven_register_reward_card_post_type() {

	$labels = array(
		'name'                => __( 'Reward Cards', 'twentyfourseven' ),
		'singular_name'       => __( 'Reward Card', 'twentyfourseven' ),
		'add_new'             => _x( 'Add New Reward Card', 'twentyfourseven', 'twentyfourseven' ),
		'add_new_item'        => __( 'Add New Reward Card', 'twentyfourseven' ),
		'edit_item'           => __( 'Edit Reward Card', 'twentyfourseven' ),
		'new_item'            => __( 'New Reward Card', 'twentyfourseven' ),
		'view_item'           => __( 'View Reward Card', 'twentyfourseven' ),
		'search_items'        => __( 'Search Reward Cards', 'twentyfourseven' ),
		'not_found'           => __( 'No Reward Cards found', 'twentyfourseven' ),
		'not_found_in_trash'  => __( 'No Reward Cards found in Trash', 'twentyfourseven' ),
		'parent_item_colon'   => __( 'Parent Reward Card:', 'twentyfourseven' ),
		'menu_name'           => __( 'Reward Cards', 'twentyfourseven' ),
	);

	$args = array(
		'labels'              => $labels,
		'hierarchical'        => false,
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-awards',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'show_in_rest'		  => true,
		'rewrite'             => array( 'slug' => 'reward-cards' ),
		'capability_type'     => 'reward_card',
		'map_meta_cap' 		  => true,
		'supports'            => array(
			'title',
			'editor',
			'thumbnail',
			'excerpt',
			'page-attributes'
		),
	);

	register_post_type( 'reward_card', $args );
}

add_action( 'init', 'twentyfourseven_register_reward_card_post_type' );

function twentyfourseven_register_promos_post_type(){

	$labels = array(
		'name'                => __( 'Promos', 'twentyfourseven' ),
		'singular_name'       => __( 'Promo', 'twentyfourseven' ),
		'add_new'             => _x( 'Add New Promo', 'twentyfourseven', 'twentyfourseven' ),
		'add_new_item'        => __( 'Add New Promo', 'twentyfourseven' ),
		'edit_item'           => __( 'Edit Promo', 'twentyfourseven' ),
		'new_item'            => __( 'New Promo', 'twentyfourseven' ),
		'view_item'           => __( 'View Promo', 'twentyfourseven' ),
		'search_items'        => __( 'Search Promos', 'twentyfourseven' ),
		'not_found'           => __( 'No Promos found', 'twentyfourseven' ),
		'not_found_in_trash'  => __( 'No Promos found in Trash', 'twentyfourseven' ),
		'parent_item_colon'   => __( 'Parent Promo:', 'twentyfourseven' ),
		'menu_name'           => __( 'Promos', 'twentyfourseven' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => null,
		'menu_icon'           => 'dashicons-megaphone',
		'show_in_nav_menus'   => false,
		'publicly_queryable'  => true,
		'exclude_from_search' => true,
		'has_archive'         => false,
		'query_var'           => true,
		'can_export'          => true,
		'show_in_rest'		  => true,
		'rewrite'             => array( 'slug' => 'promo' ),
		'capability_type'     => 'promo',
		'map_meta_cap' 		  => true,
		'supports'            => array(
			'title',
			'editor',
			'thumbnail',
		),
	);

	register_post_type( 'promo', $args );
}

add_action( 'init', 'twentyfourseven_register_promos_post_type' );

/**
 * Create a taxonomy
 *
 * @uses  Inserts new taxonomy object into the list
 * @uses  Adds query vars
 */
function twentyfourseven_register_location_services_taxonomy() {

	$labels = array(
		'name'					=> _x( 'Services', 'Taxonomy plural name', 'twentyfourseven' ),
		'singular_name'			=> _x( 'Service', 'Taxonomy singular name', 'twentyfourseven' ),
		'search_items'			=> __( 'Search Services', 'twentyfourseven' ),
		'popular_items'			=> __( 'Popular Services', 'twentyfourseven' ),
		'all_items'				=> __( 'All Services', 'twentyfourseven' ),
		'parent_item'			=> __( 'Parent Service', 'twentyfourseven' ),
		'parent_item_colon'		=> __( 'Parent Service', 'twentyfourseven' ),
		'edit_item'				=> __( 'Edit Service', 'twentyfourseven' ),
		'update_item'			=> __( 'Update Service', 'twentyfourseven' ),
		'add_new_item'			=> __( 'Add New Service', 'twentyfourseven' ),
		'new_item_name'			=> __( 'New Service Name', 'twentyfourseven' ),
		'add_or_remove_items'	=> __( 'Add or remove Services', 'twentyfourseven' ),
		'choose_from_most_used'	=> __( 'Choose from most used Services', 'twentyfourseven' ),
		'menu_name'				=> __( 'Location Services', 'twentyfourseven' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => false,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => true,
		'query_var'         => true,
		'capabilities'      => array(),
	);

	register_taxonomy( 'location_services', array( 'location' ), $args );
}

add_action( 'init', 'twentyfourseven_register_location_services_taxonomy' );

function twentyfourseven_register_reward_card_category_taxonomy() {

	$labels = array(
		'name'					=> _x( 'Categories', 'Taxonomy plural name', 'twentyfourseven' ),
		'singular_name'			=> _x( 'Category', 'Taxonomy singular name', 'twentyfourseven' ),
		'search_items'			=> __( 'Search Categories', 'twentyfourseven' ),
		'popular_items'			=> __( 'Popular Categories', 'twentyfourseven' ),
		'all_items'				=> __( 'All Categories', 'twentyfourseven' ),
		'parent_item'			=> __( 'Parent Category', 'twentyfourseven' ),
		'parent_item_colon'		=> __( 'Parent Category', 'twentyfourseven' ),
		'edit_item'				=> __( 'Edit Category', 'twentyfourseven' ),
		'update_item'			=> __( 'Update Category', 'twentyfourseven' ),
		'add_new_item'			=> __( 'Add New Category', 'twentyfourseven' ),
		'new_item_name'			=> __( 'New Category Name', 'twentyfourseven' ),
		'add_or_remove_items'	=> __( 'Add or remove Categories', 'twentyfourseven' ),
		'choose_from_most_used'	=> __( 'Choose from most used categories', 'twentyfourseven' ),
		'menu_name'				=> __( 'Categories', 'twentyfourseven' ),
	);

	$args = array(
		'labels'            => $labels,
		'public'            => true,
		'show_in_nav_menus' => true,
		'show_admin_column' => true,
		'hierarchical'      => true,
		'show_tagcloud'     => false,
		'show_ui'           => true,
		'query_var'         => true,
		'rewrite'           => array('slug' => 'reward-categories'),
		'query_var'         => true,
		'capabilities'      => array(),
	);

	register_taxonomy( 'reward_card_category', array( 'reward_card' ), $args );
}

add_action( 'init', 'twentyfourseven_register_reward_card_category_taxonomy' );

function twentyfourseven_register_job_position_post_type(){

	$labels = array(
		'name'                => __( 'Job Positions', 'twentyfourseven' ),
		'singular_name'       => __( 'Job Position', 'twentyfourseven' ),
		'add_new'             => _x( 'Add New Job Position', 'twentyfourseven', 'twentyfourseven' ),
		'add_new_item'        => __( 'Add New Job Position', 'twentyfourseven' ),
		'edit_item'           => __( 'Edit Job Position', 'twentyfourseven' ),
		'new_item'            => __( 'New Job Position', 'twentyfourseven' ),
		'view_item'           => __( 'View Job Position', 'twentyfourseven' ),
		'search_items'        => __( 'Search Job Positions', 'twentyfourseven' ),
		'not_found'           => __( 'No Job Positions found', 'twentyfourseven' ),
		'not_found_in_trash'  => __( 'No Job Positions found in Trash', 'twentyfourseven' ),
		'parent_item_colon'   => __( 'Parent Job Position:', 'twentyfourseven' ),
		'menu_name'           => __( 'Job Positions', 'twentyfourseven' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		// 'menu_position'       => null,
		'menu_icon'           => 'dashicons-paperclip',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'show_in_rest'		  => true,
		'rewrite'             => array( 'slug' => 'job-positions' ),
		// 'capability_type'     => 'job_position',
		// 'map_meta_cap' 		  => true,
		'supports'            => array(
			'title',
			'editor',
		),
	);

	register_post_type( 'job_position', $args );
}

// add_action( 'init', 'twentyfourseven_register_job_position_post_type' );

function twentyfourseven_register_job_opening_post_type(){

	$labels = array(
		'name'                => __( 'Job Openings', 'twentyfourseven' ),
		'singular_name'       => __( 'Job Opening', 'twentyfourseven' ),
		'add_new'             => _x( 'Add New Job Opening', 'twentyfourseven', 'twentyfourseven' ),
		'add_new_item'        => __( 'Add New Job Opening', 'twentyfourseven' ),
		'edit_item'           => __( 'Edit Job Opening', 'twentyfourseven' ),
		'new_item'            => __( 'New Job Opening', 'twentyfourseven' ),
		'view_item'           => __( 'View Job Opening', 'twentyfourseven' ),
		'search_items'        => __( 'Search Job Openings', 'twentyfourseven' ),
		'not_found'           => __( 'No Job Openings found', 'twentyfourseven' ),
		'not_found_in_trash'  => __( 'No Job Openings found in Trash', 'twentyfourseven' ),
		'parent_item_colon'   => __( 'Parent Job Opening:', 'twentyfourseven' ),
		'menu_name'           => __( 'Job Openings', 'twentyfourseven' ),
	);

	$args = array(
		'labels'                   => $labels,
		'hierarchical'        => false,
		'taxonomies'          => array(),
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_admin_bar'   => true,
		// 'menu_position'       => null,
		'menu_icon'           => 'dashicons-paperclip',
		'show_in_nav_menus'   => true,
		'publicly_queryable'  => true,
		'exclude_from_search' => false,
		'has_archive'         => true,
		'query_var'           => true,
		'can_export'          => true,
		'show_in_rest'		  => true,
		'rewrite'             => array( 'slug' => 'job-openings' ),
		// 'capability_type'     => 'job_position',
		// 'map_meta_cap' 		  => true,
		'supports'            => array(
			'title'
		),
	);

	register_post_type( 'job_opening', $args );
}

add_action( 'init', 'twentyfourseven_register_job_opening_post_type' );



add_filter('acf/load_field/name=attraction_map', function( $field ){

	// Template
	// $field['choices'][ $value ] = $label;

	$args = array(
		'post_type'      => 'google_maps',
		'posts_per_page' => - 1,
		'post_status'    => 'publish',
	);
	$maps = get_posts( $args );

	$field['choices'][0] = 'Choose an Attraction Map';

	foreach ( $maps as $map ){
		$field['choices'][ $map->ID ] = $map->post_title;
	}

	return $field;
} );

// add_filter( 'members_get_capabilities', function( $caps ){
// 	$caps[] = 'my_custom_247_cap';
// 	$caps[] = 'my_other_custom_247_cap';
// 	return $caps;
// } );

add_action( 'init', 'twentyfourseven_add_cap_group', 20 );

function twentyfourseven_add_cap_group(){
	if ( ! function_exists('members_register_cap_group') ){
		return;
	}

	members_register_cap_group( 'twentyfourseven',
		array(
			'label'       => esc_html__( '24/7 Custom Capabilities', 'twentyfourseven' ),
			'caps'        => array('twentyfourseven_manage_site_options', 'twentyfourseven_manage_job_openings'),
			'icon'        => 'dashicons-store',
			// 'merge_added' => false
		)
	);

}

add_action('plugins_loaded', 'twentyfourseven_add_options_pages' );

function twentyfourseven_add_options_pages(){
	if ( function_exists('acf_add_options_page') ) {

		if ( function_exists('members_register_cap_group') ){
			// Check for our custom capabilities before creating the options pages.
			if ( current_user_can('twentyfourseven_manage_site_options') ){
				acf_add_options_page( array('page_title' => 'Site Options', 'capability' => 'twentyfourseven_manage_site_options' ) );
			}
			if ( current_user_can('twentyfourseven_manage_job_openings') ){
				acf_add_options_page( array('page_title' => 'Job Openings', 'capability' => 'twentyfourseven_manage_job_openings' ) );
			}
		} else {
			// Show them all if the Members plugin is deactivated.
			acf_add_options_page('Site Options');
			acf_add_options_page('Job Openings');
		}

	}
}


// Careers Form Stuff
add_filter( 'gform_pre_render_4', 'twentyfourseven_populate_job_locations' );
add_filter( 'gform_pre_validation_4', 'twentyfourseven_populate_job_locations' );
add_filter( 'gform_pre_submission_filter_4', 'twentyfourseven_populate_job_locations' );
add_filter( 'gform_admin_pre_render_4', 'twentyfourseven_populate_job_locations' );
function twentyfourseven_populate_job_locations( $form ) {

	if ( is_admin() )
		return $form;

	foreach( $form['fields'] as $field )  {

		//NOTE: replace $field_id value with your checkbox field id
		$field_id = 2;
		if ( $field->id != $field_id ) {
			continue;
		}

		$locations = isset( $_GET['locations'] ) ? $_GET['locations'] : false ;

		natcasesort( $locations );

		if ( false === $locations )
			continue;

		$input_id = 1;

		foreach( $locations as $location ) {

			//skipping index that are multiples of 10 (multiples of 10 create problems as the input IDs)
			if ( $input_id % 10 == 0 ) {
				$input_id++;
			}

			$choices[] = array( 'text' => $location, 'value' => $location );
			$inputs[] = array( 'label' => $location, 'id' => "{$field_id}.{$input_id}" );

			$input_id++;
		}

		$field->choices = $choices;
		$field->inputs = $inputs;

	}

	return $form;
}

add_filter( 'login_redirect', function( $redirect_to, $request, $user ){
	$pos = strpos( $request, "/employee-login" );
	if ( $pos !== false ){
		$redirect_to = trailingslashit( home_url() ) . "employee-intranet";
	}
	return $redirect_to;
}, 10, 3 );

add_action('template_redirect', function(){
	if ( is_user_logged_in() && is_page(630) ){
		wp_redirect(trailingslashit( home_url() ) . "employee-intranet");
	}
});

function twentyfourseven_wpseo_fat_slicer() {
	// Only do this for users who don't have Editor capabilities
	if ( ! current_user_can( 'edit_others_posts' ) ) {
		add_action( 'add_meta_boxes', 'twentyfourseven_remove_yoast_metabox', 99 );
		add_filter( 'wp_before_admin_bar_render', 'twentyfourseven_admin_bar_seo_cleanup' );
		// add_action( 'admin_menu', 'twentyfourseven_admin_menu_cleanup' );
		// add_filter( 'manage_edit-post_columns', 'twentyfourseven_remove_columns' );
		// add_filter( 'manage_edit-page_columns', 'twentyfourseven_remove_columns' );
		// add_filter( 'manage_edit-CPTNAME_columns', 'twentyfourseven_remove_columns' ); // Replace CPTNAME with your custom post type name, for example "restaurants".
	}
}
add_action( 'init', 'twentyfourseven_wpseo_fat_slicer' );
/**
 * Removes the WordPress SEO meta box from posts and pages
 *
 * @since 1.0.0
 * @uses remove_meta_box()
 */
function twentyfourseven_remove_yoast_metabox() {
	$post_types = array( 'page', 'post', 'location', 'promo', 'reward_card', 'google_map' ); // add any custom post types here
	foreach( $post_types as $post_type ) {
		remove_meta_box( 'wpseo_meta', $post_type, 'normal' );
	}
}
/**
 * Removes the SEO item from the admin bar
 *
 * @since 1.0.0
 * @uses remove_menu
 */
function twentyfourseven_admin_bar_seo_cleanup() {
	global $wp_admin_bar;
	$wp_admin_bar->remove_menu( 'wpseo-menu' );
}

add_filter('cac/suppress_site_wide_notices', '__return_true');

/* registers bulletin board  */
function twentyfourseven_register_bulletin_post_type() {
	$labels = array(
		'name' 			=> 'Bulletin Board',
		'singular_name' => 'Bulletin Board',
		'add_new' 		=> 'Add Message',
		'all_items' 	=> 'All Bulletins',
		'add_new_item' => 'Add Bulletin',
		'edit_item' 	=> 'Edit Bulletin',
		'new_item'		=> 'New Bulletin',
		'view_item'		=> 'View Bulletin',
		'search_item'	=> 'Search Bulletins',
		'not_found'		=> 'No Bulletin found.',
		'not_found_in_trash' => 'No Bulletins found in trash.',
	);

	$args = array(
		'labels' 			 => $labels,
		'public' 			 => true,
		'has_archive' 		 => true,
		'publicly_queryable' => true,
		'query_var' 	  	 => true,
		'rewrite' 		  	 => true,
		'capability_type' 	 => 'bulletin',
		'map_meta_cap' => true,
		'hierarchical' 	  	 => false,
		'support' => array(
			'title',
			'editor',
			'excerpt',
			'thumbnail',
			'revisions',
		),
		'menu_position' => 21,
		'exclude_from_search' => false,
	);
	register_post_type('bulletin', $args);
}
add_action('init', 'twentyfourseven_register_bulletin_post_type');

/* prevent users from accessing /wp-admin */
function twentyfourseven_blockusers_init() {
	if( is_user_logged_in() ) {
		$user = wp_get_current_user();

		if ( $user->has_cap('administrator') ){
			// don't kick out admins with extra roles
			return;
		}

		if ( is_admin() &&  $user->has_cap('employee') && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
			wp_redirect( home_url() );
			exit;
		}
	}
}
add_action( 'init', 'twentyfourseven_blockusers_init' );

add_filter( 'map_meta_cap', function ( $caps, $cap, $user_id, $args ) {

	$to_filter = [ 'edit_post' ];
	$user = get_userdata( $user_id );
	$post_type = ! empty( $args ) ? get_post_type( $args[0] ) : false;
	
	// If the capability being filtered isn't of our interest, or it's not a manager, or is not editing a page, return current value
	if ( ! in_array( $cap, $to_filter, true ) || ! in_array( 'manager', $user->roles ) || $post_type !== 'page' ) {
		return $caps;
	}

	if ( in_array( 'administrator', $user->roles ) ){
		return $caps;
	}

	// First item in $args array should be page ID
	if ( $args && ! empty( $args[0] ) && $args[0] === 635 ) {
		// allow editing of page ID 635
		return [ 'exist' ];
	}
	
	// User is not allowed to edit everything else, let's tell that to WP
	return [ 'do_not_allow' ];

}, 10, 4 );

add_action('admin_menu', function(){
	global $submenu;
	$user = wp_get_current_user();

	if ( is_admin() &&  $user->has_cap('manager') && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
		$submenu['wppusher'] = false;
	}
}, 20 );

add_action('wp_dashboard_setup', function(){
	global $wp_meta_boxes;
	$user = wp_get_current_user();
	if ( is_admin() &&  $user->has_cap('manager') && ! ( defined( 'DOING_AJAX' ) && DOING_AJAX ) ) {
		$wp_meta_boxes['dashboard']['normal']['core'] = array();
	}
}, 1000 );
